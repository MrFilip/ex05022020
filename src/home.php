<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

</head>

<body>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "examen";
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "SELECT nume, categorie, imagine FROM produse";
        $result = $conn->query($sql);
    ?>
    <div class="header fixed-top">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-expand-lg navbar-custom p-0 w-100">
                    <a class="navbar-brand" href="#">
                        <img src="../images/logo.png" alt="AT Restaurant">
                    </a>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item menu-item current-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#">Pages</a>
                                <div class="dropdown-menu m-0" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#">Portofolio</a>
                                <div class="dropdown-menu m-0" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#">Blog</a>
                                <div class="dropdown-menu m-0" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#">K2 Blog</a>
                                <div class="dropdown-menu m-0" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#">Pages</a>
                                <div class="dropdown-menu m-0" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Action</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars icon-custom"></i>
                    </button>
                </nav>
            </div>
        </div>

    </div>

    <div class="welcome">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3 class="d-flex justify-content-center align-items-center">Welcome to <div
                            class="welcome-name pl-2"> at restaurant</div>
                    </h3>
                </div>
            </div>
            <div class="row my-4">
                <div class="col text-center">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-12">
                    <div class="welcome-item">
                        <div class="welcome-icon-wrapper text-center mb-3">
                            <i class="fas fa-angry welcome-icon"></i>
                        </div>
                        <div class="weclome-item-title-wrapper text-center">
                            <h3 class="weclome-item-title">Best cuisine</h3>
                        </div>
                        <div class="welcome-item-text text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue
                                semper, in dignissim.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-12">
                    <div class="welcome-item">
                        <div class="welcome-icon-wrapper text-center mb-3">
                            <i class="fas fa-angry welcome-icon"></i>
                        </div>
                        <div class="weclome-item-title-wrapper text-center">
                            <h3 class="weclome-item-title">Best cuisine</h3>
                        </div>
                        <div class="welcome-item-text text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue
                                semper, in dignissim.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-12">
                    <div class="welcome-item">
                        <div class="welcome-icon-wrapper text-center mb-3">
                            <i class="fas fa-angry welcome-icon"></i>
                        </div>
                        <div class="weclome-item-title-wrapper text-center">
                            <h3 class="weclome-item-title">Best cuisine</h3>
                        </div>
                        <div class="welcome-item-text text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue
                                semper, in dignissim.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-12">
                    <div class="welcome-item">
                        <div class="welcome-icon-wrapper text-center mb-3">
                            <i class="fas fa-angry welcome-icon"></i>
                        </div>
                        <div class="weclome-item-title-wrapper text-center">
                            <h3 class="weclome-item-title">Best cuisine</h3>
                        </div>
                        <div class="welcome-item-text text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue
                                semper, in dignissim.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="work pt-4">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h3>Latest work</h3>
                </div>
            </div>
            <div class="row my-4">
                <div class="col text-center">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et </p>
                </div>
            </div>
            <div class="row">
                <?php
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
    ?>
                <div class="col-md-3 col-sm-6 col-12 p-0 m-0">
                    <div class="category-item">
                        <img class="img-size" src="../images/<?php echo $row['imagine'] ?>"
                            alt="<?php echo $row['imagine'] ?>">
                        <div class="img-wrapper">
                            <div class="row">
                                <div class="col">
                                    <div class="denumire">
                                        <p><?php echo $row['nume'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="categorie">
                                        <p><?php echo $row['categorie'] ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
            }
        } else {
            echo "Ai gresit query-ul";
        }
        $conn->close();
    ?>
            </div>
        </div>
    </div>

    <script src="../dist/bundle.js"></script>
</body>

</html>